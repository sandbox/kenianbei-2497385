<?php

/**
 * @file
 * Admin only functionality for the media_recorder module.
 */

/**
 * Menu form callback; Display the media_recorder admin form.
 * @param $form
 * @param $form_state
 */
function media_ffmpeg_admin_form($form, $form_state) {
  $form = array();

  // Get settings.
  $settings = variable_get('media_ffmpeg', array(
    'queue' => 'cron',
    'audio' => array(
      'enable' => 'none',
      'extensions' => array(
        'wav',
        'ogg',
      ),
      'mp3' => array(
        'audio' => 0,
      ),
    ),
    'video' => array(
      'enable' => 'none',
      'extensions' => array(
        'webm',
        'mov',
      ),
      'mp4' => array(
        'video' => 0,
        'audio' => 0,
      ),
    ),
  ));

  // Get formats.
  $mp3 = new FFMpeg\Format\Audio\Mp3();
  $mp4 = new FFMpeg\Format\Video\X264();

  // Build settings array.
  $form['media_ffmpeg'] = array(
    '#type' => 'container',
    '#tree' => TRUE,
  );

  // Queue settings.
  $form['media_ffmpeg']['queue'] = array(
    '#type' => 'radios',
    '#title' => t('Convert on save or during cron?'),
    '#options' => array(
      'save' => 'On file save',
      'cron' => 'During cron',
    ),
    '#default_value' => $settings['queue'],
  );

  // Audio settings.
  $form['media_ffmpeg']['audio'] = array(
    '#type' => 'fieldset',
    '#title' => t('Audio Conversion'),
  );
  $form['media_ffmpeg']['audio']['enable'] = array(
    '#type' => 'radios',
    '#title' => t('Format'),
    '#description' => t('Convert to which format?'),
    '#options' => array(
      'none' => 'None',
      'mp3' => 'mp3',
    ),
    '#default_value' => $settings['audio']['enable'],
  );
  $form['media_ffmpeg']['audio']['extensions'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Allowed Extensions'),
    '#description' => t('Extensions to automatically convert.'),
    '#options' => array(
      'wav' => 'wav',
      'ogg' => 'ogg',
    ),
    '#default_value' => $settings['audio']['extensions'],
    '#required' => TRUE,
    '#states' => array(
      'invisible' => array(
        ':input[name="media_ffmpeg[audio][enable]"]' => array('value' => 'none'),
      )
    )
  );
  $form['media_ffmpeg']['audio']['mp3'] = array(
    '#type' => 'fieldset',
    '#title' => t('MP3'),
    '#states' => array(
      'visible' => array(
        ':input[name="media_ffmpeg[audio][enable]"]' => array('value' => 'mp3'),
      ),
    ),
  );
  $form['media_ffmpeg']['audio']['mp3']['audio'] = array(
    '#type' => 'radios',
    '#title' => t('Audio Codec'),
    '#options' => $mp3->getAvailableAudioCodecs(),
    '#default_value' => $settings['audio']['mp3']['audio'],
  );

  // Video settings.
  $form['media_ffmpeg']['video'] = array(
    '#type' => 'fieldset',
    '#title' => t('Video Format'),
  );
  $form['media_ffmpeg']['video']['enable'] = array(
    '#type' => 'radios',
    '#title' => t('Format'),
    '#description' => t('Convert to which format?'),
    '#options' => array(
      'none' => 'None',
      'mp4' => 'mp4',
    ),
    '#default_value' => $settings['video']['enable'],
  );
  $form['media_ffmpeg']['video']['extensions'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Allowed Extensions'),
    '#description' => t('Extensions to automatically convert.'),
    '#options' => array(
      'webm' => 'webm',
      'mov' => 'mov',
    ),
    '#default_value' => $settings['video']['extensions'],
    '#required' => TRUE,
    '#states' => array(
      'invisible' => array(
        ':input[name="media_ffmpeg[video][enable]"]' => array('value' => 'none'),
      )
    )
  );
  $form['media_ffmpeg']['video']['mp4'] = array(
    '#type' => 'fieldset',
    '#title' => t('MP4'),
    '#states' => array(
      'visible' => array(
        ':input[name="media_ffmpeg[video][enable]"]' => array('value' => 'mp4'),
      ),
    ),
  );
  $form['media_ffmpeg']['video']['mp4']['video'] = array(
    '#type' => 'radios',
    '#title' => t('Video Codec'),
    '#options' => $mp4->getAvailableVideoCodecs(),
    '#default_value' => $settings['video']['mp4']['video'],
  );
  $form['media_ffmpeg']['video']['mp4']['audio'] = array(
    '#type' => 'radios',
    '#title' => t('Audio Codec'),
    '#options' => $mp4->getAvailableAudioCodecs(),
    '#default_value' => $settings['video']['mp4']['audio'],
  );

  return system_settings_form($form);
}
